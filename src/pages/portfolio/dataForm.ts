export interface IInfoPortfolio {
    id: number | null,
    fullName: string;
    phoneNumber: string;
    email: string;
    description: string;
}

