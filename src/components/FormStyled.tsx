import styled from '@emotion/styled';

export const FormWrapper = styled.form`
  width: 100%;
`

export const FormItem = styled.section`
  margin-bottom: 15px;
  input {
    width: 100%;
    height: 35px;
    padding: 8px;
  }
  label {
    margin-right: 15px;
  }
`