import styled from '@emotion/styled'

export const CardStyled = styled.section`
  background-color: #FFFF;
  border-radius: 8px;
  padding: 20px;
  margin-bottom: 15px;
  color: #1a1a1a;
  width: 100%;
  height: 100%;
`