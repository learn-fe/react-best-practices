import {Portfolio} from "../pages/portfolio/Portfolio.tsx";
import {Header} from "./Header.tsx";

const Main = () => {
    return (
        <main>
            <Header/>
            <Portfolio/>
        </main>
    );
};

export default Main;
